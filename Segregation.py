import os
import shutil
from Clustering import *

source = '/home/arjun/Desktop/skylark_drones/segregation_data/stretchesCombined/'
destination = '/home/arjun/Desktop/skylark_drones/segregation_data/images/'


class CreateStretches:

    c = Clustering()

    def __init__(self, dest):

        self.dest = dest
        #print('constructor')

    def create_dir_if_not_exists(self, subdir):
        subdir_abs_path = self.dest + subdir
        print(subdir_abs_path)
        if not os.path.exists(subdir_abs_path):
            os.makedirs(subdir_abs_path)
            print('creating folder', subdir_abs_path)

    def copy_images(self, subdir, image_path):
        arr = image_path.split('.')
        src = source + arr[0].upper() + '_geotag.' + arr[1]
        if os.path.isfile(src):
            shutil.copy(src, self.dest + subdir)
            print('copying', src, 'to', self.dest + subdir)
        else:
            print(src, 'does not exist')

    def create_stretches(self):
        # stretch_data contains coordinates of region and images contained in sector
        for stretch_no, stretch_data in CreateStretches.c.Sectors.items():
            self.create_dir_if_not_exists(str(stretch_no))
            # if stretch has any images
            if len(stretch_data[:2]) > 0:
                for image_path in stretch_data[2:]:
                    self.copy_images(str(stretch_no), image_path)

createStretches = CreateStretches(destination)
createStretches.create_stretches()



